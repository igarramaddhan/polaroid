import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// import pages di sini
import Home from './pages/Home.js';
import Add from './pages/Add.js';
import Update from './pages/Update.js';
import PostContext from './context/PostContext.js';

class App extends React.Component {
  state = {
    posts: []
  };

  add = post => {
    let posts = [...this.state.posts];
    posts = [{ ...post, id: Math.random() }, ...posts];
    this.setState({ posts: posts });
  };

  update = post => {
    const posts = [...this.state.posts];
    let temp = posts.find(o => o.id === post.id);
    posts[posts.indexOf(temp)] = post;
    this.setState({ posts: posts });
  };

  delete = id => {
    let posts = [...this.state.posts];
    let post = posts.find(o => o.id === id);
    posts.splice(posts.indexOf(post), 1);
    this.setState({ posts: posts });
  };

  render() {
    return (
      <PostContext.Provider
        value={{
          posts: this.state.posts,
          add: this.add,
          update: this.update,
          delete: this.delete
        }}
      >
        <BrowserRouter>
          <Switch>
            <Route exact path='/' render={props => <Home {...props} />} />
            <Route exact path='/add' render={props => <Add {...props} />} />
            <Route
              exact
              path='/update'
              render={props => <Update {...props} />}
            />
          </Switch>
        </BrowserRouter>
      </PostContext.Provider>
    );
  }
}

export default App;
