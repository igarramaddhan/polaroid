import React from 'react';
import './Add.css';
import Layout from '../components/Layout';
import PostContext from '../context/PostContext';

export class Add extends React.Component {
  static contextType = PostContext;

  state = {
    caption: '',
    image: ''
  };

  generatePreviewImgUrl = (file, callback) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = e => callback(reader.result);
  };

  render() {
    return (
      <Layout>
        <div className='content-container'>
          <h1 className='header'>Add New Post</h1>
          <p className='sub-header'>Save every moment here</p>
          <div>
            <input
              type='file'
              onChange={e => {
                const file = e.target.files[0];
                if (file == null) {
                  return;
                }
                console.log(file);
                this.generatePreviewImgUrl(file, url => {
                  this.setState({ image: url });
                });
              }}
            />
            <textarea
              className='caption-field'
              value={this.state.caption}
              onChange={e => {
                this.setState({ caption: e.target.value });
              }}
            />
            <button
              className='button'
              onClick={() => {
                this.context.add({
                  caption: this.state.caption,
                  url: this.state.image
                });
                this.props.history.goBack();
              }}
            >
              SAVE
            </button>
          </div>
        </div>
      </Layout>
    );
  }
}

export default Add;
