import React from 'react';
import './Add.css';
import Layout from '../components/Layout';
import PostContext from '../context/PostContext';

export class Update extends React.Component {
  static contextType = PostContext;

  state = {
    caption: '',
    image: ''
  };

  componentDidMount() {
    const { caption, url } = this.props.location.state.post;
    this.setState({ caption: caption, image: url });
  }

  generatePreviewImgUrl = (file, callback) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = e => callback(reader.result);
  };

  render() {
    const post = this.props.location.state.post;
    return (
      <Layout>
        <div className='content-container'>
          <h1 className='header'>Update Post</h1>
          <p className='sub-header'>Save every moment here</p>
          <div>
            <input
              type='file'
              onChange={e => {
                const file = e.target.files[0];
                if (file == null) {
                  return;
                }
                console.log(file);
                this.generatePreviewImgUrl(file, function(url) {
                  this.setState({ image: url });
                });
              }}
            />
            <textarea
              className='caption-field'
              value={this.state.caption}
              onChange={e => {
                this.setState({ caption: e.target.value });
              }}
            />
            <button
              className='button'
              onClick={() => {
                this.context.update({
                  ...post,
                  caption: this.state.caption,
                  url: this.state.image
                });
                this.props.history.goBack();
              }}
            >
              SAVE
            </button>
          </div>
        </div>
      </Layout>
    );
  }
}

export default Update;
