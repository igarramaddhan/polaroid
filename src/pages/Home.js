import React from 'react';
import './Home.css';
import Layout from '../components/Layout';
import PostItem from '../components/PostItem';
import PostContext from '../context/PostContext';

class Home extends React.Component {
  static contextType = PostContext;

  render() {
    return (
      <Layout>
        <div className='content-container'>
          <h1 className='header'>Polaroid</h1>
          <p className='sub-header'>Save every moment here</p>
          <ul className='post-list'>
            {this.context.posts.map((val, idx) => {
              return (
                <li className='post-item' key={idx}>
                  <PostItem post={val} />
                </li>
              );
            })}
          </ul>
          <button
            className='floating-action-button'
            onClick={() => {
              this.props.history.push('/add');
            }}
          >
            +
          </button>
        </div>
      </Layout>
    );
  }
}

export default Home;
