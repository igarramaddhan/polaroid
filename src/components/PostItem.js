import React from 'react';
import './PostItem.css';
import { withRouter } from 'react-router-dom';
import PostContext from '../context/PostContext';

export class PostItem extends React.Component {
  static contextType = PostContext;

  render() {
    console.log(this.props);
    return (
      <div className='post-item-container'>
        <img alt='post' src={this.props.post.url} />
        <p className='caption'>{this.props.post.caption}</p>
        <button
          className='post-button'
          onClick={() => {
            this.props.history.push('/update', { post: this.props.post });
          }}
        >
          UPDATE
        </button>
        <button
          className='post-button'
          onClick={() => {
            this.context.delete(this.props.post.id);
          }}
        >
          DELETE
        </button>
      </div>
    );
  }
}

export default withRouter(PostItem);
